const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 

// Q1. Find all the movies with total earnings more than $500M.
function earningsMoreThan(movies, earning) {
    return Object.keys(movies).reduce((accu, movie) => {
            if(Number.parseInt(movies[movie].totalEarnings.substring(1,4)) > earning) {
            accu[movie] = movies[movie]
        }

        return accu
    },{})
}

const earningsAbove500 = earningsMoreThan(favouritesMovies, 500)
// console.log(earningsAbove500)

// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
function earningsMoreThanAndOscarNom(movies, earning, noms) {
    return Object.keys(movies).reduce((accu, movie) => {
            if(Number.parseInt(movies[movie].totalEarnings.substring(1,4)) > earning && movies[movie].oscarNominations > noms) {
            accu[movie] = movies[movie]
        }

        return accu
    },{})
}

const moviesWth3Noms = earningsMoreThanAndOscarNom(favouritesMovies, 500, 3)
// console.log(moviesWth3Noms)

// Q.3 Find all movies of the actor "Leonardo Dicaprio".
function moviesWithActor(movies, actor) {
    return Object.keys(movies).reduce((accu, movie) => {
        if(movies[movie].actors.includes(actor)) {
            accu[movie] = movies[movie]
        }
        return accu
    },{})
}

const moviesWithLeo = moviesWithActor(favouritesMovies, "Leonardo Dicaprio")
// console.log(moviesWithLeo)

// Q.4 Sort movies (based on IMDB rating)
function sortByRating(movies) {
    return Object.keys(movies).sort((movie1, movie2) => {
        if(movies[movie2].imdbRating === movies[movie1].imdbRating) {
            return Number.parseInt(movies[movie2].totalEarnings.substring(1,4) - Number.parseInt(movies[movie1].totalEarnings.substring(1,4)))
        }

        return movies[movie2].imdbRating - movies[movie1].imdbRating
    })
    .map((movie) => {
        let movieObj = {}
        movieObj[movie] = movies[movie]
        return movieObj
    })
}

const sortedMovies = sortByRating(favouritesMovies)
// console.log(sortedMovies)

// Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
// drama > sci-fi > adventure > thriller > crime
function groupByGenre(movies) {
    return Object.keys(movies).reduce((accu, movie) => {
        if(movies[movie].genre.includes('drama')) {
            if(accu['drama'] === undefined) {
                accu['drama'] = {}
            }
            accu['drama'][movie] = movies[movie]
        } else if(movies[movie].genre.includes('sci-fi')) {
            if(accu['sci-fi'] === undefined) {
                accu['sci-fi'] = {}
            }
            accu['sci-fi'][movie] = movies[movie]
        } else if(movies[movie].genre.includes('adventure')) {
            if(accu['adventure'] === undefined) {
                accu['adventure'] = {}
            }
            accu['adventure'][movie] = movies[movie]
        } else if(movies[movie].genre.includes('thriller')) {
            if(accu['thriller'] === undefined) {
                accu['thriller'] = {}
            }
            accu['thriller'][movie] = movies[movie]
        } else if(movies[movie].genre.includes('crime')) {
            if(accu['crime'] === undefined) {
                accu['crime'] = {}
            }
            accu['crime'][movie] = movies[movie]
        }
        return accu
    },{})
}

const groupedByGenre = groupByGenre(favouritesMovies)
console.log(groupedByGenre)