const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 

// Q1 Find all users who are interested in playing video games.
function getUsersInterestedInGames(users = {}) {
    return Object.keys(users).filter((user) => {
        return users[user].interests.toString().toLowerCase().includes("video games")
    })
    .reduce((accu, user) => {
        accu[user] = users[user]
        return accu
    },{})
}

const interestedInGames = getUsersInterestedInGames(users)
// console.log(interestedInGames)

// Q2 Find all users staying in Germany.
function usersByCountry(users = {}, country) {
    return Object.keys(users).filter((user) => {
        return users[user].nationality === country
    })
    .reduce((accu, user) => {
        accu[user] = users[user]
        return accu
    },{})
}

const usersInGermany = usersByCountry(users, "Germany")
// console.log(usersInGermany)

// Q3 Sort users based on their seniority level 
function sortBySeniority(users) {
    return Object.keys(users).sort((user1, user2) => {
        if(users[user2].desgination.includes("Senior Developer")) {
            if(users[user1].desgination.includes("Senior Developer")) {
                return users[user2].age - users[user1].age
            }
            return 1
        }

        if(users[user2].desgination.includes("Developer")) {
            if(users[user1].desgination.includes("Senior Developer")) {
                return -1
            }

            if(users[user1].desgination.includes("Developer")) {
                return users[user2].age - users[user1].age
            }

            return 1
        }

        if(users[user2].desgination.includes("Intern")) {
            if(users[user1].desgination.includes("Senior Developer")) {
                return -1
            }

            if(users[user1].desgination.includes("Developer")) {
                return -1
            }

            if(users[user1].desgination.includes("Intern")) {
                return users[user2].age - users[user1].age
            }
        }
    })
    .map((user) => {
        let userObj = {}
        userObj[user] = users[user]
        return userObj
    })
}

const usersBySeniority = sortBySeniority(users)
// console.log(usersBySeniority)

// Q4 Find all users with masters Degree.
function usersByDegree(users = {}, degree) {
    return Object.keys(users).filter((user) => {
        return users[user].qualification === degree
    })
    .reduce((accu, user) => {
        accu[user] = users[user]
        return accu
    },{})
}

const usersWithMasters = usersByDegree(users, "Masters")
// console.log(usersWithMasters)

// Q5 Group users based on their Programming language mentioned in their designation.
function groupByLangauge(users = {}) {
    return Object.keys(users).reduce((accu, user) => {
        
        if(users[user].desgination.includes("Javascript")) {
            let userObj = accu["Javascript"]
            userObj[user] = users[user]
            accu["Javascript"] = userObj
        }
        if(users[user].desgination.includes("Golang")) {
            let userObj = accu["Golang"]
            userObj[user] = users[user]
            accu["Golang"] = userObj
        }
        if(users[user].desgination.includes("Python")) {
            let userObj = accu["Python"]
            userObj[user] = users[user]
            accu["Python"] = userObj
        }

        return accu
    },{
        "Javascript":{},
        "Golang":{},
        "Python":{}
    })
}

const groupedByLangauge = groupByLangauge(users)
console.log(groupedByLangauge)
